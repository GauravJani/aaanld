//
//  HomeTableCell.swift
//  NewApp
//
//  Created by Gaurang Mistry on 27/10/17.
//  Copyright © 2017 Gaurang Mistry. All rights reserved.
//

import UIKit

class HomeTableCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var btn_Share: UIButton!
  
    @IBOutlet weak var shrOut: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
