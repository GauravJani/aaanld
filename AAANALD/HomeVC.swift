//
//  HomeVC.swift
//  NewApp
//
//  Created by Gaurang Mistry on 16/10/17.
//  Copyright © 2017 Gaurang Mistry. All rights reserved.
//

import UIKit
import Google

class HomeVC: UIViewController {

    @IBAction func btnMartes(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NewMartesViewController") as! NewMartesViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBOutlet var btnMartes: UIButton!
    
    @IBOutlet weak var img_ic_News: UIImageView!
    @IBOutlet weak var imgConjos: UIImageView!
    @IBOutlet weak var imgDire: UIImageView!
    @IBOutlet weak var imgCirculars: UIImageView!
    @IBOutlet weak var imgChat: UIImageView!
    @IBOutlet weak var imgContacto: UIImageView!
    @IBOutlet weak var imgHome: UIImageView!
    @IBOutlet weak var imgImformation: UIImageView!
    @IBOutlet weak var lblHome: UILabel!
    @IBOutlet weak var lblImformation: UILabel!
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var constraint_ViewBottomHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        print("Did Load Method")
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.navigationBar.isHidden = true
        
        app_Delegate.setImageColor(imgConjos         , strName: "users", color: "ThemeColor")
        app_Delegate.setImageColor(imgDire            , strName: "user", color: "ThemeColor")
        app_Delegate.setImageColor(imgCirculars       , strName: "note", color: "ThemeColor")
        app_Delegate.setImageColor(imgChat         , strName: "message", color: "ThemeColor")
        app_Delegate.setImageColor(imgContacto , strName: "user-circle", color: "ThemeColor")
        app_Delegate.setImageWhiteColor(imgHome , strName: "home", color: "WhiteColor")
        app_Delegate.setImageGrayColor(imgImformation, strName: "info", color: "GrayColor")
        app_Delegate.setImageColor(img_ic_News, strName: "ic_News", color: "ThemeColor")
        
        
        
//       img_ic_News.image = img_ic_News.image?.withRenderingMode(.alwaysTemplate)
//       img_ic_News.tintColor = UIColor.darkText

        
        lblHome.textColor = UIColor.white
        lblImformation.textColor = UIColor.lightGray
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("Will appear method")
        //=======================Goolge Analytics======================================//
        print("Screen Name---------->>", kHomeScreen)
        guard let tracker = GAI.sharedInstance().defaultTracker else { return }
        tracker.set(kGAIScreenName, value: kHomeScreen)
        guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
        tracker.send(builder.build() as [NSObject : AnyObject])
        //======================================================================//
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    // MARK: - UIButton Action
    @IBAction func btnInformationActionClk(_ sender: Any) {
        let objInfo = self.storyboard?.instantiateViewController(withIdentifier: "InformationVC") as! InformationVC
        self.navigationController?.pushViewController(objInfo, animated: false)
    }

}

