//
//  CircularesVC.swift
//  NewApp
//
//  Created by Gaurang Mistry on 26/10/17.
//  Copyright © 2017 Gaurang Mistry. All rights reserved.
//

import UIKit
import Google

class CircularesVC: UIViewController, UITextFieldDelegate, ServerCallDelegate {

    var strUDID = ""
    var strRemOrNot = ""
    var strGcm_Token = ""
    @IBOutlet var viewLoginBg: UIView!
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtPass: UITextField!
    @IBOutlet var btnRemeber: UIButton!
    @IBOutlet var btnForgotPass: UIButton!
    @IBOutlet var lblUnderLineForgot: UILabel!
    @IBOutlet var btnLogin: UIButton!
    @IBOutlet var btnLogout: UIButton!
    @IBOutlet var lblLogoutUnderline: UILabel!
    @IBOutlet var btnRegiscirculars: UIButton!
    @IBOutlet var constraing_login_view_height: NSLayoutConstraint!
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var constraint_ViewBottomHeight: NSLayoutConstraint!
    @IBOutlet var constraint_login_top: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        txtPass.delegate = self
        txtEmail.delegate = self
        btnForgotPass.isHidden = true
        lblUnderLineForgot.isHidden = true
        
        if _userDefault.bool(forKey: "login_user") == true {
            btnLogout.isHidden = false
            lblLogoutUnderline.isHidden = false
            btnRegiscirculars.isHidden = false
            viewLoginBg.isHidden = true
            constraing_login_view_height.constant = 0
        }
        else {
            if _userDefault.bool(forKey: "Remember") == true {
                txtEmail.text = _userDefault.string(forKey: "Email")
                txtPass.text = _userDefault.string(forKey: "Pass")
                btnRemeber.isSelected = true
                strRemOrNot = "YES"
            }
            else {
                strRemOrNot = "NO"
                txtEmail.text = ""
                txtPass.text = ""
            }
            btnLogout.isHidden = true
            lblLogoutUnderline.isHidden = true
            btnRegiscirculars.isHidden = true
            viewLoginBg.isHidden = false
            constraing_login_view_height.constant = 375
       }
        constraint_ViewBottomHeight.constant = 480
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
       // strUDID = OpenUDID.value()
        strUDID = UIDevice.current.identifierForVendor!.uuidString
        
        self.navigationController?.isNavigationBarHidden = true
        
        //=======================Goolge Analytics======================================//
        print("Screen Name---------->>", kThirdOption)
        guard let tracker = GAI.sharedInstance().defaultTracker else { return }
        tracker.set(kGAIScreenName, value: kThirdOption)
        guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
        tracker.send(builder.build() as [NSObject : AnyObject])
        //======================================================================//
     }

    // MARK: - Login Api Call Method
    func CallApiForLogin() {
        if (_userDefault.string(forKey: "fcm") != nil) {
            strGcm_Token = _userDefault.string(forKey: "fcm")!
        }
        else {
            strGcm_Token = "1234567890IOS"
        }
        
        // init paramters Dictionary
        let myUrl = kBasePath + "login"
        
        let param = ["username"    : txtEmail.text!,
                     "password"    : txtPass.text!,
                     "type"        : "1",
                     "gcm_id"      : strGcm_Token,
                     "device_id"   : String(strUDID)]
        
        print(myUrl, param)
        
        ServerCall.sharedInstance.requestWithUrlAndParameters(.POST, urlString: myUrl, parameters: param as [String : AnyObject], delegate: self, name: .serverCallNameLogin)
    }
    
    // MARK: - Call API For Logout
        func CallApiForLogOut() {
        
        // init paramters Dictionary
        let myUrl = kBasePath + "logOut"
        
        let param = ["user_id"     : _userDefault.integer(forKey: "user_id").description]
        
        print(myUrl, param)
        
        ServerCall.sharedInstance.requestWithUrlAndParameters(.POST, urlString: myUrl, parameters: param as [String : AnyObject], delegate: self, name: .serverCallNameLogout)
    }
    
    
    // MARK: - Server Call Delegate
    func ServerCallSuccess(_ resposeObject: AnyObject, name: ServerCallName) {
        print(resposeObject)
        var dicData = resposeObject as! [AnyHashable : Any]
        
        if name == ServerCallName.serverCallNameLogin {
            
            if ((dicData["success"]) != nil) {
                app_Delegate.stopLoadingView()
                // Create the alert controller
                let strResponse = TO_INT(dicData["success"]);
                
                if strResponse == 1 {
                    
                    let resData = resposeObject["result"] as? NSDictionary
                    
                    let email = TO_STRING(resData?["email"])
                    let FName = TO_STRING(resData?["first_name"])
                    let LName = TO_STRING(resData?["last_name"])
                    let userID = TO_INT(resData?["user_id"])
                    
                    _userDefault.set(email, forKey: "user_email")
                    _userDefault.set(FName, forKey: "user_fname")
                    _userDefault.set(LName, forKey: "user_lname")
                    _userDefault.set(userID, forKey: "user_id")
                    
                    if strRemOrNot == "YES" {
                        _userDefault.set(txtEmail.text!, forKey: "Email")
                        _userDefault.set(txtPass.text!, forKey: "Pass")
                        _userDefault.set(true, forKey: "Remember")
                    }
                    else {
                        _userDefault.set("", forKey: "Email")
                        _userDefault.set("", forKey: "Pass")
                        _userDefault.set(false, forKey: "Remember")

                    }
                    _userDefault.set(true, forKey: "login_user")
                    
                    app_Delegate.stopLoadingView()
                    let objHome = self.storyboard?.instantiateViewController(withIdentifier: "GetMainDataVC") as! GetMainDataVC
                    self.navigationController?.pushViewController(objHome, animated: true)
                    
                }
                else {
                    let strMsg = TO_STRING(dicData["msg"]);
                    app_Delegate.stopLoadingView()
                    Constant.showAlert(title: "", message: strMsg)
                    return
                }
            }
            else {
                let strerrMsg = TO_STRING(dicData["error"])
                Constant.showAlert(title: "", message:strerrMsg)
            }
        }
        
        else if name == ServerCallName.serverCallNameLogout {
            
            if ((dicData["success"]) != nil) {
                app_Delegate.stopLoadingView()
                // Create the alert controller
                let strResponse = TO_INT(dicData["success"]);
                
                if strResponse == 1 {
                    app_Delegate.stopLoadingView()
                    
                    _userDefault.set(false, forKey: "login_user")
                    _ = self.navigationController?.popViewController(animated: true)
                }
                else {
                    let strMsg = TO_STRING(dicData["msg"]);
                    app_Delegate.stopLoadingView()
                    Constant.showAlert(title: "", message: strMsg)
                    return
                }
            }
            else {
                let strerrMsg = TO_STRING(dicData["error"])
                Constant.showAlert(title: "", message:strerrMsg)
            }
        }
        app_Delegate.stopLoadingView()
    }
    
    
    // MARK: - Server Failed Delegate
    func ServerCallFailed(_ errorObject: String, name: ServerCallName) {
        app_Delegate.stopLoadingView()
        Constant.showAlert(title: "", message:errorObject)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - TextField Delegate Method
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtEmail {
            textField.resignFirstResponder()
            txtPass.becomeFirstResponder()
            return false
        }
        textField.resignFirstResponder()
        return true
    }
    
    // MARK: - Validation function
    func isValidData() -> Bool{
        if !(txtEmail?.text!.isStringWithoutSpace())!{
            Constant.showAlert(title: "", message: "Por favor ingrese su nombre de usuario y correo electrónico" )
            return false;
        }
//        else if !(txtEmail?.text!.isEmail())!{
//            Constant.showAlert(title: "", message: "Por favor introduzca un correo electrónico válido")
//            return false;
//        }
        else if !(txtPass?.text!.isStringWithoutSpace())!{
            Constant.showAlert(title: "", message: "Porfavor ingrese una Contraseña" )
            return false;
        }
        
        return true
    }
    
    // MARK: - Button Action
    @IBAction func btnRememberAction(_ sender: UIButton) {
        if btnRemeber.isSelected == true {
            strRemOrNot = "NO"
            btnRemeber.isSelected = false
        }
        else {
            strRemOrNot = "YES"
            btnRemeber.isSelected = true
        }
    }

    @IBAction func btnForgotPassAction(_ sender: UIButton) {
        self.view.endEditing(true)
        let objHome = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
        self.navigationController?.pushViewController(objHome, animated: true)
    }
    
    
    // MARK: - Button Login Button Action
    @IBAction func btnLoginAction(_ sender: UIButton) {
        if isValidData() == false {
            return
        }
        else{
            self.view.endEditing(true)
            //Call Api For Login
            app_Delegate.startLoadingview("")
            self.CallApiForLogin()
        }
    }
    
    // MARK: - Button Logout Button Action
    @IBAction func btnLogOutAction(_ sender: UIButton) {
        self.view.endEditing(true)
    ///////////////Gaurav////////////////
        
        app_Delegate.startLoadingview("")
        self.CallApiForLogOut()

        popoverPresentationController?.sourceView = btnLogout

    ////////////End///////////////
       // Call Api For LogOut
//        let actionSheetPhoto = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
//
//        let actionLogout = UIAlertAction(title: "Salir", style: .default) { (action : UIAlertAction) in
//
//            app_Delegate.startLoadingview("")
//            self.CallApiForLogOut()
//        }
//
//        let actionCancel = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
//
//        actionSheetPhoto.addAction(actionLogout)
//        actionSheetPhoto.addAction(actionCancel)
//
//      actionSheetPhoto.popoverPresentationController?.sourceView = btnLogout
//        self.present(actionSheetPhoto, animated: true, completion: nil)
    }
    
    @IBAction func btnSignUpAction(_ sender: UIButton) {
        self.view.endEditing(true)
        let objRegiser = self.storyboard?.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
        self.navigationController?.pushViewController(objRegiser, animated: true)
    }
    
    @IBAction func btnRegisterCircularAction(_ sender: UIButton) {
        let objHome = self.storyboard?.instantiateViewController(withIdentifier: "GetMainDataVC") as! GetMainDataVC
        self.navigationController?.pushViewController(objHome, animated: true)
    }
    
    @IBAction func clkToBackAction(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }

}
