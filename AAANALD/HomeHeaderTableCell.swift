//
//  HomeHeaderTableCell.swift
//  NewApp
//
//  Created by Gaurang Mistry on 27/10/17.
//  Copyright © 2017 Gaurang Mistry. All rights reserved.
//

import UIKit

class HomeHeaderTableCell: UITableViewCell {

    @IBOutlet weak var lblHeader: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
