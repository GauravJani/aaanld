//
//  ContactTableCell.swift
//  NewApp
//
//  Created by Gaurang Mistry on 26/10/17.
//  Copyright © 2017 Gaurang Mistry. All rights reserved.
//

import UIKit

class ContactTableCell: UITableViewCell {

    @IBOutlet weak var lblmobile2: UILabel!
    @IBOutlet weak var btnMobile2: UIButton!
    @IBOutlet weak var btnemailTop: NSLayoutConstraint!
    @IBOutlet weak var lblMainTitle: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnMobile: UIButton!
    @IBOutlet weak var btnEmail: UIButton!
    @IBOutlet weak var lblTime: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
