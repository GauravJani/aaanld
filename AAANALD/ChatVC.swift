//
//  ChatVC.swift
//  AAANLD
//
//  Created by Gaurang Mistry on 01/11/17.
//  Copyright © 2017 Gaurang Mistry. All rights reserved.
//

import UIKit
import Google

class ChatVC: UIViewController, UIWebViewDelegate {

    @IBOutlet var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        webView.delegate = self
        
          app_Delegate.startLoadingview("")
        //let url = URL (string: kChatLink)
        
       //let url = URL (string: "https://marvelapp.com/446gb6f/screen/40727379")
        
        let url = URL (string: "http://www.aaanldapp.org/API/chat.php")
        
        let requestObj = URLRequest(url: url!)
        
        webView.loadRequest(requestObj)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //=======================Goolge Analytics======================================//
        print("Screen Name---------->>", kChat)
        guard let tracker = GAI.sharedInstance().defaultTracker else { return }
        tracker.set(kGAIScreenName, value: kChat)
        guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
        tracker.send(builder.build() as [NSObject : AnyObject])
        //======================================================================//
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
      app_Delegate.stopLoadingView()
    }

    @IBAction func clkToBackAction(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
}
