//
//  ForgotPasswordVC.swift
//  NewApp
//
//  Created by Gaurang Mistry on 26/10/17.
//  Copyright © 2017 Gaurang Mistry. All rights reserved.
//

import UIKit

class ForgotPasswordVC: UIViewController, UITextFieldDelegate, ServerCallDelegate {

    @IBOutlet var viewLoginBg: UIView!
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var btnSubmit: UIButton!
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var constraint_ViewBottomHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        txtEmail.delegate = self
    }

    
    // MARK: - Forgot Password Api Call Method
    func CallApiForForgotPassword() {
        // init paramters Dictionary
        let myUrl = kBasePath + "confirmForgetPass"
        
        let param = ["email"       : txtEmail.text!]
        
        print(myUrl, param)
        
        ServerCall.sharedInstance.requestWithUrlAndParameters(.POST, urlString: myUrl, parameters: param as [String : AnyObject], delegate: self, name: .serverCallNameForgotPass)
    }
    
    
    // MARK: - Server Call Delegate
    func ServerCallSuccess(_ resposeObject: AnyObject, name: ServerCallName) {
        print(resposeObject)
        var dicData = resposeObject as! [AnyHashable : Any]
        
        if name == ServerCallName.serverCallNameForgotPass {
            
            if ((dicData["success"]) != nil) {
                app_Delegate.stopLoadingView()
                // Create the alert controller
                let strResponse = TO_INT(dicData["success"]);
                let strMsg = TO_STRING(dicData["msg"]);
                
                if strResponse == 1 {
                    
                    let alertController = UIAlertController(title: "", message: strMsg, preferredStyle: .alert)
                    
                    // Create the actions
                    let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
                        UIAlertAction in
                        _ = self.navigationController?.popViewController(animated: true)
                        NSLog("OK Pressed")
                    }
                    
                    // Add the actions
                    alertController.addAction(okAction)
                    
                    // Present the controller
                    self.present(alertController, animated: true, completion: nil)
                    
                }
                else {
                    app_Delegate.stopLoadingView()
                    Constant.showAlert(title: "", message: strMsg)
                    return
                }
            }
            else {
                let strerrMsg = TO_STRING(dicData["error"])
                Constant.showAlert(title: "", message:strerrMsg)
            }
        }
        app_Delegate.stopLoadingView()
    }
    
    
    // MARK: - Server Failed Delegate
    func ServerCallFailed(_ errorObject: String, name: ServerCallName) {
        app_Delegate.stopLoadingView()
        Constant.showAlert(title: "", message:errorObject)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: - TextField Delegate Method
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // MARK: - Validation function
    func isValidData() -> Bool{
        if !(txtEmail?.text!.isStringWithoutSpace())!{
            Constant.showAlert(title: "", message: "introduce un nombre de usuario o dirección de correo electrónico" )
            return false;
        }
        else if !(txtEmail?.text!.isEmail())!{
            Constant.showAlert(title: "", message: "Por favor introduzca un correo electrónico válido")
            return false;
        }
        return true
    }
    
    
    @IBAction func btnSubmitAction(_ sender: UIButton) {
        if isValidData() == false {
            return
        }
        else{
            self.view.endEditing(true)
            //Call Api For Login
            app_Delegate.startLoadingview("")
            self.CallApiForForgotPassword()
        }
    }
    
    @IBAction func clkToBackAction(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
}

