//
//  DirectorioVC.swift
//  NewApp
//
//  Created by Gaurang Mistry on 26/10/17.
//  Copyright © 2017 Gaurang Mistry. All rights reserved.
//

import UIKit
import MessageUI
import Google

class DirectorioVC: UIViewController, UITableViewDataSource, UITableViewDelegate, MFMailComposeViewControllerDelegate {

    var strMailTxt = ""
    var screenWidth = CGFloat()
    var screenHeight = CGFloat()
    var arrDirectorio = NSMutableArray()
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var constraint_ViewBottomHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let screenSize = UIScreen.main.bounds
        screenWidth = screenSize.width
        screenHeight = screenSize.height
        
        self.setArrayForDisplay()
        //===================================================================//
        tblView.estimatedRowHeight = 80.0
        tblView.rowHeight = UITableViewAutomaticDimension
        
        //Table Cell Register
        tblView.register(UINib(nibName: "ContactTableCell", bundle: nil), forCellReuseIdentifier: "ContactTableCell")
        //===================================================================//
        
        tblView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //=======================Goolge Analytics======================================//
        print("Screen Name---------->>", kSecondOption)
        guard let tracker = GAI.sharedInstance().defaultTracker else { return }
        tracker.set(kGAIScreenName, value: kSecondOption)
        guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
        tracker.send(builder.build() as [NSObject : AnyObject])
        //======================================================================//
    }
    
    func setArrayForDisplay() {
        //
        let paramFirst = ["mainTitle" : "Presidencia",
                          "name"      : "Norma A. Torres Rocha",
                          "mobile"    : "(867) 711-5803",
                          "email"     : "ntorres@aaanld.org",
                          "time"      : "9:00AM - 6:30PM"]
        arrDirectorio.add(paramFirst)
        //
        let paramSecond = ["mainTitle" : "Contabilidad",
                           "name"       : "C.P. Sonia Treviño Salinas",
                           "mobile"     : "(867) 711-5804",
                           "email"      : "strevino@aaanld.org",
                           "time"       : "9:00AM - 6:30PM"]
        arrDirectorio.add(paramSecond)
        //
        let paramThird = ["mainTitle" : "Administración",
                          "name"      : "Diana L. Rangel Vargas",
                          "mobile"    : "(867) 711-5800",
                          "email"     : "drangel@aaanld.org",
                          "time"      : "9:00AM - 7:00PM"]
        arrDirectorio.add(paramThird)
        //
        let paramSix = ["mainTitle" : "Patronato de Damas",
                        "name"    : "Alba N. Castillo",
                        "mobile"  : "711-5815 Ext. 4118",
                        "email"   : "acastillo@aaanld.org",
                        "time"    : "9:00AM - 6:30PM"]
        arrDirectorio.add(paramSix)
        
        let paramFourth = ["mainTitle" : "Asesoría Aduanera",
                           "name"       : "Gricelda Gómez Hernández",
                           "mobile"     : "(867) 711-5805",
                           "email"      : "ggomez@aaanld.org",
                           "time"       : "9:00AM - 7:00PM"]
        arrDirectorio.add(paramFourth)
        //
        let paramFive = ["mainTitle" : "Asesoría en Puente de Comercio Mundial",
                         "name"     : "Lic. Ana Laura Longoria Chapa",
                         "mobile"   : "(867) 724-2000",
                         "email"    : "alongoria@aaanld.org",
                         "time"     : "9:00AM - 9:00PM"]
        arrDirectorio.add(paramFive)
        //
      
        //
        let paramSeven = ["mainTitle" : "Asesoria Jurídica",
                          "name"      : "Lic.Valeria Medina",
                          "mobile"    : "(867) 711-5810",
                          "email"     : "vmedina@aaanld.org",
                          "time"      : "9:00AM - 7:00PM"]
        arrDirectorio.add(paramSeven)
        //
        let paramEight = ["mainTitle" : "Estación Sánchez",
                          "name"      : "Ricardo Coss García\n(Multidependiente SAGARPA) \n(867) 129-19-38 \n \n Lic. Francisco Chávez García\n(Multidependiente Aduana)",
                          "mobile"    : "(867) 100-0012\n(867) 711-5110 ext 6153, 6154",
                          "email"     : "multidependiente@aaanld.org",
                          "time"      : "9:00AM - 7:00PM"]
        arrDirectorio.add(paramEight)
        //
       
        //
        let paramTen = ["mainTitle" : "Exportación Puente III",
                        "name"    : "Francisco G. Ortíz",
                        "mobile"  : "(867) 193 2107 y ID 32*24*67645",
                        "email"   : "fortiz@aaanld.org",
                        "time"    : "7:00AM - 12:00PM"]
        arrDirectorio.add(paramTen)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    
    //=================================================================================================//
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        // Return the number of sections.
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        return arrDirectorio.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactTableCell", for: indexPath as IndexPath) as! ContactTableCell
        
        cell.backgroundColor = UIColor.clear
        cell.selectionStyle = .none
        cell.separatorInset = UIEdgeInsets.zero
        
        var dict: NSDictionary = [:]
        dict = arrDirectorio.object(at:indexPath.row) as! NSDictionary
        
        let strTitle = dict["mainTitle"] as! String
        cell.lblMainTitle.text = strTitle
        
        let strName = dict["name"] as! String
        cell.lblName.text = strName
        
        let strMobile = dict["mobile"] as! String
        if strMobile.contains(find:"\n")
        {
            cell.btnMobile2.isHidden = false
            cell.btnemailTop.constant = 25
            cell.lblmobile2.isHidden = false
            let arr = strMobile.components(separatedBy:"\n")
            print(arr)
           
             cell.btnMobile2.setTitle(arr[1], for: .normal)
             cell.btnMobile.setTitle(arr[0], for: .normal)
           
        }
        else
        {
            cell.lblmobile2.isHidden = true
             cell.btnMobile.setTitle(strMobile, for: .normal)
            cell.btnMobile2.isHidden = true
            cell.btnemailTop.constant = 2
            
        }
       
        
        let strEmail = dict["email"] as! String
        cell.btnEmail.setTitle(strEmail, for: .normal)
        
        let strTime = dict["time"] as! String
        cell.lblTime.text = strTime
        
        
        cell.btnMobile.tag = indexPath.row
        cell.btnMobile.addTarget(self, action: #selector(self.clkToMobileTapedFromCell), for: .touchUpInside)
        cell.btnMobile2.tag = indexPath.row
        cell.btnMobile2.addTarget(self, action: #selector(self.clkToMobileTapedFromCell22), for: .touchUpInside)

        cell.btnEmail.tag = indexPath.row
        cell.btnEmail.addTarget(self, action: #selector(self.clkToEmailTapedFromCell), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    
    //======================================Click On Mobile=========================================//
    @IBAction func clkToMobileTapedFromCell(_ sender: UIButton) {
        let btnMobile = (sender as AnyObject).convert(CGPoint.zero, to: tblView)
        let index = tblView.indexPathForRow(at: btnMobile)!
        
        var dict: NSDictionary = [:]
        dict = arrDirectorio.object(at:index.row) as! NSDictionary
        
        let strMobile = dict["mobile"] as! String
        if strMobile.contains(find: "\n")
        {
            let arr = strMobile.components(separatedBy:"\n")
            print(arr)
            app_Delegate.makeaCall(arr[0])
            print(strMobile)
        }
        else
        {
        app_Delegate.makeaCall(strMobile)
        print(strMobile)
        }
    }
    @IBAction func clkToMobileTapedFromCell22(_ sender: UIButton) {
        let btnMobile = (sender as AnyObject).convert(CGPoint.zero, to: tblView)
        let index = tblView.indexPathForRow(at: btnMobile)!
        
        var dict: NSDictionary = [:]
        dict = arrDirectorio.object(at:index.row) as! NSDictionary
        
        let strMobile = dict["mobile"] as! String
        if strMobile.contains(find: "\n")
        {
            let arr = strMobile.components(separatedBy:"\n")
            print(arr)
            let strfinal = arr[1].components(separatedBy: "ext")
             print(strfinal[0])
            app_Delegate.makeaCall(strfinal[0])
            print(strMobile)
        }
        else
        {
            app_Delegate.makeaCall(strMobile)
            print(strMobile)
        }
    }
    //==============================================================================================//
    
    //======================================Click On Email=========================================//
    @IBAction func clkToEmailTapedFromCell(_ sender: UIButton) {
        let btnEmail = (sender as AnyObject).convert(CGPoint.zero, to: tblView)
        let index = tblView.indexPathForRow(at: btnEmail)!
        
        var dict: NSDictionary = [:]
        dict = arrDirectorio.object(at:index.row) as! NSDictionary
        
        strMailTxt = dict["email"] as! String
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
        print(strMailTxt)
    }
    //=================================================================================================//
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients([strMailTxt])
        mailComposerVC.setSubject("")
        mailComposerVC.setMessageBody("", isHTML: false)
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        let alertController = UIAlertController(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "Okay", style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("OK Pressed")
        }
        
        // Add the actions
        alertController.addAction(okAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK: MFMailComposeViewControllerDelegate Method
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    //=================================================================================================//
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func clkToBackAction(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }


}











