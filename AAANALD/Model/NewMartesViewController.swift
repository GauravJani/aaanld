//
//  NewMartesViewController.swift
//  AAANALD
//
//  Created by Saturncube on 25/09/18.
//  Copyright © 2018 Gaurang Mistry. All rights reserved.
//

import UIKit

class NewMartesViewController: UIViewController,UIWebViewDelegate {

    @IBAction func btnRefresh(_ sender: Any) {
        app_Delegate.startLoadingview("")
        let url = URL(string: "https://aaanld.org/educacion-y-capacitacion/presentaciones-martes-de-consulta/")
        // turn it into a request and use NSData to load its content
        let requestObj = URLRequest(url: url!)
        self.Out_Webview.loadRequest(requestObj)
        
    }
    @IBAction func btnBack(_ sender: Any) {
        
        self.navigationController!.popViewController(animated: true)
    }
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var Out_Webview: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
       self.Out_Webview.delegate = self
        app_Delegate.startLoadingview("")
        let url = URL(string: "https://aaanld.org/educacion-y-capacitacion/presentaciones-martes-de-consulta/")
        // turn it into a request and use NSData to load its content
        let requestObj = URLRequest(url: url!)
        self.Out_Webview.loadRequest(requestObj)
        
        
        // Do any additional setup after loading the view.
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
    
        app_Delegate.stopLoadingView()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
