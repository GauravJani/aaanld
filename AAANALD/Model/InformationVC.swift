//
//  InformationVC.swift
//  NewApp
//
//  Created by Gaurang Mistry on 27/10/17.
//  Copyright © 2017 Gaurang Mistry. All rights reserved.
//

import UIKit
import Google

class InformationVC: UIViewController, UIScrollViewDelegate {

    var screenWidth = CGFloat()
    var screenHeight = CGFloat()
    var selection = 0
    var interestedX = CGFloat()
    var arrImage = NSMutableArray()
    @IBOutlet weak var lblHome: UILabel!
    @IBOutlet weak var lblImformation: UILabel!
    @IBOutlet weak var imgHome: UIImageView!
    @IBOutlet weak var imgImformation: UIImageView!
    @IBOutlet var objscrlloView: UIScrollView!
    @IBOutlet var PageControl: UIPageControl!
    @IBOutlet var btnRegistro: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        PageControl.isHidden = true
        
        //Set Array
        arrImage = []
        //=====================================//
        
        app_Delegate.setImageWhiteColor(imgHome       , strName: "home", color: "WhiteColor")
        app_Delegate.setImageWhiteColor(imgImformation, strName: "info", color: "WhiteColor")
        
        app_Delegate.setImageGrayColor(imgHome, strName: "home", color: "GrayColor")
        app_Delegate.setImageWhiteColor(imgImformation , strName: "info", color: "WhiteColor")
        
        lblHome.textColor = UIColor.lightGray
        lblImformation.textColor = UIColor.white
        
        let screenSize = UIScreen.main.bounds
        screenWidth = screenSize.width
        screenHeight = screenSize.height
        objscrlloView.backgroundColor = UIColor.clear
        
        PageControl.currentPage = 0
        PageControl.numberOfPages = arrImage.count
        
        
        
        //.text = String(sel) + " of " + String(arrImage.count)
        
        //SET SELECTED INDEX
        
        //....................  SET FOLLOWING PEOPLE ARE INTERESTED .............................//
        objscrlloView.showsHorizontalScrollIndicator = false
        objscrlloView.showsVerticalScrollIndicator = false
        objscrlloView.delegate = self
        interestedX = objscrlloView.frame.origin.x
        
        for i in 0..<arrImage.count {
            
            let imgPic = UIImageView(frame: CGRect(x: interestedX, y: -20.0, width: screenWidth, height: screenHeight))
            imgPic.backgroundColor = UIColor.clear
            imgPic.contentMode = .scaleAspectFit
            imgPic.image = arrImage[i] as? UIImage
            
            objscrlloView.addSubview(imgPic)
            interestedX = interestedX + screenWidth
            print("\(interestedX)")
            
            
            
        }
        //SET THE SCROLL VIEW WIDHT
        objscrlloView.contentSize = CGSize(width: interestedX, height: 400)
        objscrlloView.isPagingEnabled = true
        
        let XPosition = Int(screenWidth) * selection
        objscrlloView.setContentOffset(CGPoint(x: XPosition, y: 0), animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //=======================Goolge Analytics======================================//
        print("Screen Name---------->>", kInforamtionScreen)
        guard let tracker = GAI.sharedInstance().defaultTracker else { return }
        tracker.set(kGAIScreenName, value: kInforamtionScreen)
        guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
        tracker.send(builder.build() as [NSObject : AnyObject])
        //======================================================================//
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //........................ UIScrollView Delegate...........................................//
    
    // MARK: - UIScrollView Delegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let fractionalPage: Float = Float(objscrlloView.contentOffset.x) / Float(screenWidth)
        //    scrollviewPageIndex=
        
        print(String(lround(Double(fractionalPage))))
        
        let selVal = Int(lround(Double(fractionalPage)))
        //selVal = selVal + 1
        
        PageControl.currentPage = selVal
        //) + " of " + String(arrImage.count)
        //objPageControl.currentPage = lround(Double(fractionalPage))
    }
    
    
    
    // MARK: - UIButton Action
    @IBAction func btnHomeActionClk(_ sender: Any) {
        let objHome = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        self.navigationController?.pushViewController(objHome, animated: false)
    }
    
    
    @IBAction func btnRegistroActionClk(_ sender: Any) {
        let objRegiser = self.storyboard?.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
        objRegiser.strScreenFrom = "Register"
        self.navigationController?.pushViewController(objRegiser, animated: true)
    }
    
    
    
}


