//
//  PDFViewerVC.swift
//  NewApp
//
//  Created by Gaurang Mistry on 27/10/17.
//  Copyright © 2017 Gaurang Mistry. All rights reserved.
//

import UIKit
import Google

class PDFViewerVC: UIViewController, UIWebViewDelegate {

    var strTitle = ""
    var strPDFLink = ""
    var strFilepat = ""
    var strScreenFrom = ""
    @IBOutlet var pdfView: UIWebView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var imgShare: UIImageView!
    @IBOutlet var imgDownload: UIImageView!
    @IBOutlet var btnShare: UIButton!
    @IBOutlet var btnDownload: UIButton!
    @IBOutlet var imgProduct: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        print(strPDFLink)
        lblTitle.text = strTitle
        
        if strScreenFrom == "PDFView" {
            imgDownload.isHidden = true
            btnDownload.isHidden = true
            pdfView.isHidden = false
            imgProduct.isHidden = true
            
            app_Delegate.startLoadingview("")
            Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.LoadPDFView), userInfo: self, repeats: false)
        }
        else {
            imgDownload.isHidden = false
            btnDownload.isHidden = false
            pdfView.isHidden = true
            imgProduct.isHidden = false
            let url = URL(string:strPDFLink)
            imgProduct.sd_setImage(with: url as URL!, placeholderImage: nil, options: []) { (image, error, imageCacheType, imageUrl) in
                if image != nil
                {
                    self.imgProduct.image = image
                }
            }
        }
    }
    
    @objc func LoadPDFView() {
        
        let url = URL(string: strPDFLink)
        // turn it into a request and use NSData to load its content
        let reqquest = URLRequest(url: url!)
        let data: Data? = try? NSURLConnection.sendSynchronousRequest(reqquest, returning: nil)
        // find Documents directory and append your local filename
        var documentsURL: URL? = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last
        documentsURL = documentsURL?.appendingPathComponent("file.pdf")
        // and finally save the file
        try! data?.write(to: documentsURL!, options: Data.WritingOptions.atomic)
        
        let GetdocumentsURL: URL? = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last
        let contents = try? FileManager.default.contentsOfDirectory(at: GetdocumentsURL!, includingPropertiesForKeys: nil, options: .skipsHiddenFiles)
        print("\(String(describing: contents?.description))")
        
        let strLocalPDFpath = contents?[0]
        print(strLocalPDFpath ?? 00)
        strFilepat = (strLocalPDFpath?.description)!
        strFilepat.remove(at: (strFilepat.startIndex))
        strFilepat.remove(at: (strFilepat.startIndex))
        strFilepat.remove(at: (strFilepat.startIndex))
        strFilepat.remove(at: (strFilepat.startIndex))
        strFilepat.remove(at: (strFilepat.startIndex))
        strFilepat.remove(at: (strFilepat.startIndex))
        strFilepat.remove(at: (strFilepat.startIndex))
        print(strFilepat )
        
        // file saved
        let shareLink = NSURL(fileURLWithPath: strFilepat)
        let requestObj = URLRequest(url: shareLink as URL)
        pdfView.loadRequest(requestObj)
//        let strUrlLink = "https://docs.google.com/gview?embedded=true&url=" + strPDFLink
//        let purl = URL (string: strUrlLink)
//        let requestObj = URLRequest(url: purl!)
//        pdfView.loadRequest(requestObj)
        app_Delegate.stopLoadingView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //=======================Goolge Analytics======================================//
        print("Screen Name---------->>", lblTitle.text!)
        guard let tracker = GAI.sharedInstance().defaultTracker else { return }
        tracker.set(kGAIScreenName, value: lblTitle.text!)
        guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
        tracker.send(builder.build() as [NSObject : AnyObject])
        //======================================================================//
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func webViewDidFinishLoad(_ webView: UIWebView) {
        app_Delegate.stopLoadingView()
    }
    
    // MARK: - UIScrollView Delegate Methods
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        pdfView.scrollView.maximumZoomScale = 20
        // set similar to previous.
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func clkTobtnShareAction(_ sender: UIButton) {
       
        if strScreenFrom == "PDFView" {
            
            // file saved
            let pdfLink = NSURL(fileURLWithPath: strFilepat)
            let objectsToShare = [pdfLink] //comment!, imageData!, myWebsite!]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            if UI_USER_INTERFACE_IDIOM() == .phone {
                app_Delegate.stopLoadingView()
                self.present(activityVC, animated: true) {  }
            }
            else {
                // Change Rect to position Popover
                app_Delegate.stopLoadingView()
                let popup = UIPopoverController(contentViewController: activityVC)
                popup.present(from: CGRect(x: view.frame.size.width / 2, y: view.frame.size.height / 4, width: 0, height: 0), in: view, permittedArrowDirections: .any, animated: true)
            }
        }
        else {
            self.ShareImage()
        }
    }
    
    func ShareImage() {
        // image to share
        if strScreenFrom == "PDFView" {
        }
        else {
            let url = URL(string: self.strPDFLink)
            let data = try? Data(contentsOf: url!)
            if data != nil {
                let image = UIImage(data: data!)
                let imageToShare = [ image! ]
                let avc = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
                //if iPhone
                if UI_USER_INTERFACE_IDIOM() == .phone {
                    present(avc, animated: true) { }
                }
                else {
                    // Change Rect to position Popover
                    let popup = UIPopoverController(contentViewController: avc)
                    popup.present(from: CGRect(x: view.frame.size.width / 2, y: view.frame.size.height / 4, width: 0, height: 0), in: view, permittedArrowDirections: .any, animated: true)
                }
            }
        }
    }
    
    
    @IBAction func clkTobtnDownloadAction(_ sender: UIButton) {

        let actionSheetPhoto = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let actionDownload = UIAlertAction(title: "Guardar imagen", style: .default) { (action : UIAlertAction) in
            
            let url = URL(string: self.strPDFLink)
            let data = try? Data(contentsOf: url!)
            if data != nil {
                let image = UIImage(data: data!)
            
                
                UIImageWriteToSavedPhotosAlbum(image!, self, #selector(PDFViewerVC.thisImage(_:hasBeenSavedInPhotoAlbumWithError:usingContextInfo:)), nil)
                app_Delegate.startLoadingview("")
            }
            
        }
        
        let actionCancel = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        
        actionSheetPhoto.addAction(actionDownload)
        actionSheetPhoto.addAction(actionCancel)
        
        actionSheetPhoto.popoverPresentationController?.sourceView = btnDownload
        self.present(actionSheetPhoto, animated: true, completion: nil)
    }
    
    
    @objc func thisImage(_ image: UIImage, hasBeenSavedInPhotoAlbumWithError error: Error?, usingContextInfo ctxInfo: UnsafeMutableRawPointer) {
        if error != nil {
            print("error")
            app_Delegate.stopLoadingView()
        }
        else {
            print("Saved to Gallary")
        }
        app_Delegate.stopLoadingView()
    }

    @IBAction func clkToBackAction(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
}
