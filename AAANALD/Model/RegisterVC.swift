//
//  RegisterVC.swift
//  NewApp
//
//  Created by Gaurang Mistry on 27/10/17.
//  Copyright © 2017 Gaurang Mistry. All rights reserved.
//

import UIKit
import Google

class RegisterVC: UIViewController, UITextFieldDelegate, ServerCallDelegate {
    
    var strScreenFrom = ""
    @IBOutlet var txtFirstName: UITextField!
    @IBOutlet var txtLastName: UITextField!
    @IBOutlet var txtUserName: UITextField!
    @IBOutlet var txtPatent: UITextField!
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtPass: UITextField!
    @IBOutlet var txtConfirmPass: UITextField!
    @IBOutlet var btnRegister: UIButton!
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var constraint_ViewBottomHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        txtPass.delegate = self
        txtEmail.delegate = self
        txtUserName.delegate = self
        txtPatent.delegate = self
        txtLastName.delegate = self
        txtFirstName.delegate = self
        txtConfirmPass.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        //=======================Goolge Analytics======================================//
        print("Screen Name---------->>", kRegister)
        guard let tracker = GAI.sharedInstance().defaultTracker else { return }
        tracker.set(kGAIScreenName, value: kRegister)
        guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
        tracker.send(builder.build() as [NSObject : AnyObject])
        //======================================================================//
    }
    
    // MARK: - Register Api Call Method
    func CallApiForRegisterUser() {
        // init paramters Dictionary
        let myUrl = kBasePath + "register"
        
        let param = ["email"      : txtEmail.text!,
                     "username"   : txtUserName.text!,
                     "patent"     : txtPatent.text!,
                     "password"   : txtPass.text!,
                     "last_name"  : txtLastName.text!,
                     "first_name" : txtFirstName.text!]
        
        print(myUrl, param)
        
        ServerCall.sharedInstance.requestWithUrlAndParameters(.POST, urlString: myUrl, parameters: param as [String : AnyObject], delegate: self, name: .serverCallNameRegister)
    }
    
    
    // MARK: - Server Call Delegate
    func ServerCallSuccess(_ resposeObject: AnyObject, name: ServerCallName) {
        print(resposeObject)
        var dicData = resposeObject as! [AnyHashable : Any]
        
        if name == ServerCallName.serverCallNameRegister {
            
            if ((dicData["success"]) != nil) {
                app_Delegate.stopLoadingView()
                // Create the alert controller
                let strResponse = TO_INT(dicData["success"]);
                let strMsg = TO_STRING(dicData["msg"]);
                
                if strResponse == 1 {
                    
                    let alertController = UIAlertController(title: "", message: strMsg, preferredStyle: .alert)
                    
                    // Create the actions
                    let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
                        UIAlertAction in
                        
                        
                        if self.strScreenFrom == "Register" {
                            let objLogin = self.storyboard?.instantiateViewController(withIdentifier: "CircularesVC") as! CircularesVC
                            self.navigationController?.pushViewController(objLogin, animated: true)
                        }
                        else {
                            _ = self.navigationController?.popViewController(animated: true)
                        }
                        
                        
                        
                        
                        NSLog("OK Pressed")
                    }
                    
                    // Add the actions
                    alertController.addAction(okAction)
                    
                    // Present the controller
                    self.present(alertController, animated: true, completion: nil)
                    
                }
                else {
                    app_Delegate.stopLoadingView()
                    Constant.showAlert(title: "", message: strMsg)
                    return
                }
            }
            else {
                let strerrMsg = TO_STRING(dicData["error"])
                Constant.showAlert(title: "", message:strerrMsg)
            }
        }
        app_Delegate.stopLoadingView()
    }
    
    
    // MARK: - Server Failed Delegate
    func ServerCallFailed(_ errorObject: String, name: ServerCallName) {
        app_Delegate.stopLoadingView()
        Constant.showAlert(title: "", message:errorObject)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    // MARK: - TextField Delegate Method
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtFirstName {
            textField.resignFirstResponder()
            txtLastName.becomeFirstResponder()
            return false
        }
        if textField == txtLastName {
            textField.resignFirstResponder()
            txtUserName.becomeFirstResponder()
            return false
        }
        if textField == txtUserName {
            textField.resignFirstResponder()
            txtPatent.becomeFirstResponder()
            return false
        }
        if textField == txtPatent {
            textField.resignFirstResponder()
            txtEmail.becomeFirstResponder()
            return false
        }
        if textField == txtEmail {
            textField.resignFirstResponder()
            txtPass.becomeFirstResponder()
            return false
        }
        if textField == txtPass {
            textField.resignFirstResponder()
            txtConfirmPass.becomeFirstResponder()
            return false
        }
        textField.resignFirstResponder()
        return true
    }
    
    // MARK: - Validation function
    func isValidData() -> Bool{
        if !(txtFirstName?.text!.isStringWithoutSpace())!{
            Constant.showAlert(title: "", message: "Por favor ingrese el nombre" )
            return false;
        }
        else if !(txtLastName?.text!.isStringWithoutSpace())!{
            Constant.showAlert(title: "", message: "Por favor ingrese el apellido" )
            return false;
        }
        else if !(txtUserName?.text!.isStringWithoutSpace())!{
            Constant.showAlert(title: "", message: "Por favor, ingrese su nombre de usuario" )
            return false;
        }
        else if !(txtPatent?.text!.isStringWithoutSpace())!{
            Constant.showAlert(title: "", message: "Por favor ingrese su patente" )
            return false;
        }
        else if !(txtEmail?.text!.isStringWithoutSpace())!{
            Constant.showAlert(title: "", message: "Por favor ingrese correo electrónico" )
            return false;
        }
        else if !(txtEmail?.text!.isEmail())!{
            Constant.showAlert(title: "", message: "Por favor introduzca un correo electrónico válido")
            return false;
        }
        else if !(txtPass?.text!.isStringWithoutSpace())!{
            Constant.showAlert(title: "", message: "Por favor, ingrese contraseña" )
            return false;
        }
        else if !(txtConfirmPass?.text!.isStringWithoutSpace())!{
            Constant.showAlert(title: "", message: "Por favor ingrese Confirmar contraseña" )
            return false;
        }
        else if !(txtPass?.text! == txtConfirmPass.text) {
            Constant.showAlert(title: "", message: "Por favor ingrese una contraseña válida de confirmación" )
            return false;
        }
        
        return true
    }
    
    
    // MARK: - Button Login Button Action
    @IBAction func btnLoginAction(_ sender: UIButton) {
        if isValidData() == false {
            return
        }
        else{
            self.view.endEditing(true)
            //Call Api For Login
            app_Delegate.startLoadingview("")
            self.CallApiForRegisterUser()
        }
    }
    
    
    @IBAction func clkToBackAction(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
}

