//
//  GetMainDataVC.swift
//  NewApp
//
//  Created by Gaurang Mistry on 26/10/17.
//  Copyright © 2017 Gaurang Mistry. All rights reserved.
//

import UIKit
import Google

class GetMainDataVC: UIViewController, ServerCallDelegate, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate,  ReaderViewControllerDelegate,UIPickerViewDelegate,UIPickerViewDataSource{
   
    var selectdRow = 0
    var strFiltered = ""
    var selectedValue = ""
    var txtSelectBus = UITextField()
    var toolBar = UIToolbar()
    var pickerView: UIPickerView = UIPickerView()
    var resData = NSDictionary()
    var arrKeys = NSMutableArray()
    @IBOutlet var btnFilter: UIButton!
    @IBOutlet var tblView: UITableView!
    @IBOutlet var txtSearch: UITextField!
    @IBOutlet var imgSearch: UIImageView!
    @IBOutlet var lblNoResult: UILabel!
    @IBOutlet var txtHide: UITextField!
    @IBOutlet var imgHide: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        //app_Delegate.setImageColor(, strName: "ic_News", color: "ThemeColor")
        
        strFiltered = "NO"
        lblNoResult.isHidden = true
        app_Delegate.setImageGrayColor(imgSearch, strName: "magnifier", color: "GrayColor")
        
        //Set Picker
        pickerView.dataSource = self
        pickerView.delegate  = self
        toolBar.isTranslucent = false
        toolBar.tintColor = UIColor.black
        toolBar.barStyle = UIBarStyle.default
        toolBar.barTintColor = Constant.blackColor()
         pickerView.reloadAllComponents();
        let btnCancel = UIButton(type: .custom)
        btnCancel.setTitle("Reset", for: .normal)
        btnCancel.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(60), height: CGFloat(25))
        btnCancel.addTarget(self, action: #selector(self.cancelPressed), for: .touchUpInside)
        let cancelButton = UIBarButtonItem(customView: btnCancel)
        
        let btnDone = UIButton(type: .custom)
        btnDone.setTitle("Done", for: .normal)
        btnDone.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(60), height: CGFloat(25))
        btnDone.addTarget(self, action: #selector(self.donePressed), for: .touchUpInside)
        let doneButton = UIBarButtonItem(customView: btnDone)
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        //==================================================================================//
        
        
        tblView.register(UINib(nibName: "HomeTableCell", bundle: nil), forCellReuseIdentifier: "HomeTableCell")
        
        tblView.register(UINib(nibName: "HomeHeaderTableCell", bundle: nil), forCellReuseIdentifier: "HomeHeaderTableCell")
        
        tblView.estimatedRowHeight = 80
        tblView.rowHeight = UITableViewAutomaticDimension
        
        app_Delegate.startLoadingview("")
        self.CallApiForGetHomePageData()
        
        //For Search
        txtSearch.delegate = self
        txtSearch.clearButtonMode = .whileEditing
        txtSearch.addTarget(self, action: #selector(self.txtSearchData), for: .editingChanged)
        txtSearch.text = ""
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        //=======================Goolge Analytics======================================//
        print("Screen Name---------->>", kMainScreen)
        guard let tracker = GAI.sharedInstance().defaultTracker else { return }
        tracker.set(kGAIScreenName, value: kMainScreen)
        guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
        tracker.send(builder.build() as [NSObject : AnyObject])
        //======================================================================//
    }

    // MARK: - Home Page Data Api Call Method
    func CallApiForGetHomePageData() {

        // init paramters Dictionary
        let myUrl = kBasePath + "getCricularIOS"
        
        let param = ["user_id" : _userDefault.integer(forKey: "user_id").description]
        
        print(myUrl, param)
        
        ServerCall.sharedInstance.requestWithUrlAndParameters(.POST, urlString: myUrl, parameters: param as [String : AnyObject], delegate: self, name: .serverCallNameGetHomeData)
    }
    
    
    // MARK: - Server Call Delegate
    func ServerCallSuccess(_ resposeObject: AnyObject, name: ServerCallName) {
        print(resposeObject)
        var dicData = resposeObject as! [AnyHashable : Any]
        
        if name == ServerCallName.serverCallNameGetHomeData {
            
            if ((dicData["success"]) != nil) {
                app_Delegate.stopLoadingView()
                // Create the alert controller
                let strResponse = TO_INT(dicData["success"]);
                
                if strResponse == 1 {
                    
                    let resssData = resposeObject["result"] as? NSArray
                    if resssData != nil {
                        
                        let arrPostData = (resposeObject["result"]! as! NSArray).mutableCopy() as! NSMutableArray
                        print(arrPostData);
                        
                        if arrPostData.count == 0 {
                             _userDefault.removeObject(forKey: "resData")
                            imgHide.isHidden = true
                            txtHide.isHidden = true
                            txtSearch.isHidden = true
                            btnFilter.isHidden = true
                            lblNoResult.isHidden = false
                            
                        }
                        else {
                            imgHide.isHidden = false
                            txtHide.isHidden = false
                            txtSearch.isHidden = false
                            btnFilter.isHidden = false
                            lblNoResult.isHidden = true
                            
                            arrKeys = (resposeObject["key"]! as! NSArray).mutableCopy() as! NSMutableArray
                            print(arrKeys);
                            
                            resData = (resposeObject["result"] as? NSDictionary)!
                            print("\(resData.count)")
                            print(resData)
                            
                            let dataSave = NSKeyedArchiver.archivedData(withRootObject: resData)
                            _userDefault.set(dataSave, forKey: "resData")
                            
                            let dataKSave = NSKeyedArchiver.archivedData(withRootObject: arrKeys)
                            _userDefault.set(dataKSave, forKey: "arrKeys")
                            
                            
                            tblView.reloadData()
                        }
                    }
                    else {
                        imgHide.isHidden = false
                        txtHide.isHidden = false
                        txtSearch.isHidden = false
                        lblNoResult.isHidden = true
                        
                        arrKeys = (resposeObject["key"]! as! NSArray).mutableCopy() as! NSMutableArray
                        print(arrKeys);
                        
                        resData = (resposeObject["result"] as? NSDictionary)!
                        
                        print("\(resData.count)")
                        print(resData)
                        
                        let dataSave = NSKeyedArchiver.archivedData(withRootObject: resData)
                        _userDefault.set(dataSave, forKey: "resData")
                        
                        let dataKSave = NSKeyedArchiver.archivedData(withRootObject: arrKeys)
                        _userDefault.set(dataKSave, forKey: "arrKeys")
                        
                        tblView.reloadData()
                    }
                
                }
                else {
                     _userDefault.removeObject(forKey: "resData")
                     _userDefault.removeObject(forKey: "arrKeys")
                    let strMsg = TO_STRING(dicData["msg"]);
                    app_Delegate.stopLoadingView()
                    Constant.showAlert(title: "", message: strMsg)
                    return
                }
            }
            else {
                 _userDefault.removeObject(forKey: "resData")
                _userDefault.removeObject(forKey: "arrKeys")
                let strerrMsg = TO_STRING(dicData["error"])
                Constant.showAlert(title: "", message:strerrMsg)
            }
        }
        app_Delegate.stopLoadingView()
        pickerView.reloadAllComponents();
    }
    
    
    // MARK: - Server Failed Delegate
    func ServerCallFailed(_ errorObject: String, name: ServerCallName) {
         _userDefault.removeObject(forKey: "resData")
        _userDefault.removeObject(forKey: "arrKeys")
        app_Delegate.stopLoadingView()
        Constant.showAlert(title: "", message:errorObject)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //==================================================================================================//
    //==================================================================================================//
    //==================================================================================================//
    
    ///////////////Gaurav ////////////////////
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrKeys.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        //        var sectionString = Array(resData.allKeys)[row] as? String
        //        let sectionArray = Array(resData.allKeys)[row]
        var sectionString = arrKeys[row] as? String
        let sectionArray = arrKeys[row]
        let Count =  (resData[sectionArray]! as AnyObject).count
        sectionString = sectionString! + " (" + (Count?.description)! + ")"
        return sectionString?.capitalized
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectdRow = row
        selectedValue = arrKeys[row] as! String
        //selectedValue = Array(resData.allKeys)[row] as! String
    }
    
    ////////////////End///////////////////////
    
    // MARK: - PickerView Delegate Dipak Bhai
//    func numberOfComponentsInPickerView(pickerView: UIPickerView!) -> Int{
//        return 1
//    }
    
    // returns the # of rows in each component..
//    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
//        //return resData.count
//        return arrKeys.count
//    }
    
//    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
////        var sectionString = Array(resData.allKeys)[row] as? String
////        let sectionArray = Array(resData.allKeys)[row]
//        var sectionString = arrKeys[row] as? String
//        let sectionArray = arrKeys[row]
//        let Count =  (resData[sectionArray]! as AnyObject).count
//        sectionString = sectionString! + " (" + (Count?.description)! + ")"
//        return sectionString?.capitalized
//    }
    
//    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
//        selectdRow = row
//        selectedValue = arrKeys[row] as! String
//        //selectedValue = Array(resData.allKeys)[row] as! String
//    }
    //==================================================================================================//
    
    //==================================================================================================//
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // Return the number of sections.
        return resData.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "HomeHeaderTableCell") as? HomeHeaderTableCell else {
            return UIView()
        }
        
        cell.contentView.backgroundColor = UIColor.lightGray
        
//        var sectionString = Array(resData.allKeys)[section] as! String
//        let sectionArray = Array(resData.allKeys)[section]
//        let Count =  (resData[sectionArray]! as AnyObject).count
//        sectionString = sectionString + " (" + (Count?.description)! + ")"
//        cell.lblHeader.text = sectionString.capitalized
        
        var sectionString = arrKeys[section] as! String
        let sectionArray = arrKeys[section]
        let Count =  (resData[sectionArray]! as AnyObject).count
        sectionString = sectionString + " (" + (Count?.description)! + ")"
        cell.lblHeader.text = sectionString.capitalized
        
        return cell.contentView
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //let sectionString = Array(resData.allKeys)[section]
        let sectionString = arrKeys[section]
        return (resData[sectionString]! as AnyObject).count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let Iticell = tableView.dequeueReusableCell(withIdentifier: "HomeTableCell", for: indexPath as IndexPath) as! HomeTableCell
        
        Iticell.btn_Share.isHidden = true
        
        Iticell.selectionStyle = .none
        Iticell.separatorInset = UIEdgeInsets.zero
        
        
//        let sectionString = Array(resData.allKeys)[indexPath.section]
//        let arrData = resData[sectionString] as AnyObject
        
        let sectionString = arrKeys[indexPath.section]
        let arrData = resData[sectionString] as AnyObject
        
        var dict: NSDictionary = [:]
        dict = arrData.object(at:indexPath.row) as! NSDictionary
        
        let strPdflnk = TO_STRING(dict["circular_title"])
        //Iticell.lblTitle.text = strPdflnk
        
        let underlineAttribute = [NSAttributedStringKey.underlineStyle: NSUnderlineStyle.styleSingle.rawValue]
        let underlineAttributedString = NSAttributedString(string: strPdflnk, attributes: underlineAttribute)
        Iticell.lblTitle.attributedText = underlineAttributedString
        
        let strDate = TO_STRING(dict["crt_date"])
        Iticell.lblDate.text = strDate
        
        
        Iticell.lblTitle.tag = indexPath.row
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapFunction))
        Iticell.lblTitle.isUserInteractionEnabled = true
        Iticell.lblTitle.addGestureRecognizer(tap)
        
        
        return Iticell;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension;
    }
    
    
    //
    @objc func tapFunction(_ sender: UITapGestureRecognizer) {
        let tapPoint = sender.location(in: tblView)
        let tapPointInView = tblView.convert(tapPoint, to: tblView)
        let indexpa = tblView.indexPathForRow(at: tapPointInView)!
        //let sectionString = Array(resData.allKeys)[indexpa.section]
        let sectionString = arrKeys[indexpa.section]
        let arrData = resData[sectionString] as AnyObject
        var dict: NSDictionary = [:]
        dict = arrData.object(at:indexpa.row) as! NSDictionary
        
        let strFileName = TO_STRING(dict["file_name"])
        let arrFile = strFileName.components(separatedBy: ".")
        let strLastObject = arrFile.last
        let strTitle = TO_STRING(dict["circular_title"])
        var titlepdf = strTitle.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        titlepdf = titlepdf.removeWhitespace()
        titlepdf = titlepdf + ".pdf"
        
        if strLastObject == "pdf" {
            let objPDF = self.storyboard?.instantiateViewController(withIdentifier: "PDFViewerVC") as! PDFViewerVC
            objPDF.strScreenFrom = "PDFView"
            objPDF.strPDFLink = TO_STRING(dict["file_name"])
            objPDF.strTitle = TO_STRING(dict["circular_title"])
            self.navigationController?.pushViewController(objPDF, animated: true)
        }
        else {
            let objPDF = self.storyboard?.instantiateViewController(withIdentifier: "PDFViewerVC") as! PDFViewerVC
            objPDF.strScreenFrom = "ImageView"
            objPDF.strPDFLink = TO_STRING(dict["file_name"])
            objPDF.strTitle = TO_STRING(dict["circular_title"])
            self.navigationController?.pushViewController(objPDF, animated: true)
        }
    }
    
    //==============//====================//============//======================//====================//
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // Mark: - For Search
    //==================================Fot Search=============================================//
    @objc func txtSearchData() {
        if (txtSearch.text == "") {
            if strFiltered == "NO" {
                if _userDefault.value(forKey: "resData") != nil {
                    let dataarrCategory: Data? = _userDefault.data(forKey: "resData")
                    resData = NSKeyedUnarchiver.unarchiveObject(with: dataarrCategory!) as! NSDictionary
                    
                    let dataKCategory: Data? = _userDefault.data(forKey: "arrKeys")
                    arrKeys = NSKeyedUnarchiver.unarchiveObject(with: dataKCategory!) as! NSMutableArray
                    
                    if resData.count == 0 {
                        lblNoResult.isHidden = false
                        
                    }
                    else {
                        lblNoResult.isHidden = true
                    }
                    
                    tblView.reloadData()
                }
            }
            else {
                if _userDefault.value(forKey: "resTempData") != nil {
                    let dataarrCategory: Data? = _userDefault.data(forKey: "resTempData")
                    resData = NSKeyedUnarchiver.unarchiveObject(with: dataarrCategory!) as! NSDictionary
                    
                    let dataaKeysCategory: Data? = _userDefault.data(forKey: "KeysTempData")
                    arrKeys = NSKeyedUnarchiver.unarchiveObject(with: dataaKeysCategory!) as! NSMutableArray
                    
                    if resData.count == 0 {
                        lblNoResult.isHidden = false
                        
                    }
                    else {
                        lblNoResult.isHidden = true
                    }
                }
                tblView.reloadData()
                
            }
        }
        else {
            if _userDefault.value(forKey: "resData") != nil {
                var TempResData = NSDictionary()
                var TempKeys = NSMutableArray()
                if strFiltered == "NO" {
                    let dataarrCategory: Data? = _userDefault.data(forKey: "resData")
                    TempResData = NSKeyedUnarchiver.unarchiveObject(with: dataarrCategory!) as! NSDictionary
                    
                    let dataKeyCategory: Data? = _userDefault.data(forKey: "arrKeys")
                    TempKeys = NSKeyedUnarchiver.unarchiveObject(with: dataKeyCategory!) as! NSMutableArray
                }
                else {
                    let dataarrCategory: Data? = _userDefault.data(forKey: "resTempData")
                    TempResData = NSKeyedUnarchiver.unarchiveObject(with: dataarrCategory!) as! NSDictionary
                    
                    let datKeysrCategory: Data? = _userDefault.data(forKey: "KeysTempData")
                    TempKeys = NSKeyedUnarchiver.unarchiveObject(with: datKeysrCategory!) as! NSMutableArray
                }
                methodSearch(TempResData, keysOfArray: TempKeys)
                tblView.reloadData()
            }
        }
    }
    
    
    
    func methodSearch(_ searchDict: NSDictionary, keysOfArray: NSMutableArray) {
        
        //var arrTempData = NSMutableArray()
        print(keysOfArray)
        arrKeys = NSMutableArray()
        let DicresData = NSMutableDictionary()
        
        for i in 0..<searchDict.count {
            //let sectionString = Array(searchDict.allKeys)[i] as? String
            //let sectionArray = Array(searchDict.allKeys)[i]
            let sectionString = keysOfArray[i] as? String
            let sectionArray = keysOfArray[i]
            let arrData = searchDict[sectionArray] as AnyObject
            
            if (sectionArray as! NSString).range(of: txtSearch.text!, options: .caseInsensitive).location != NSNotFound {
                if DicresData[sectionArray] != nil {
                }
                else {
                    arrKeys.add(sectionArray)
                    DicresData.setValue(arrData, forKey: sectionString!)
                }
                
            }
            
            for j in 0..<arrData.count {
                var dict: NSDictionary = [:]
                dict = arrData.object(at:j) as! NSDictionary
                
                let Title = TO_STRING(dict["circular_title"])
                let Date = TO_STRING(dict["crt_date"])
                
                if (Title as NSString).range(of: txtSearch.text!, options: .caseInsensitive).location != NSNotFound {
                    if DicresData[sectionArray] != nil {
                        let arrTempData = DicresData[sectionArray] as AnyObject
                        if arrTempData.contains(dict) {
                        }
                        else {
                            arrTempData.add(dict)
                        }
                        arrKeys.remove(sectionArray)
                        DicresData.removeObject(forKey: sectionArray)
                        arrKeys.add(sectionArray)
                        DicresData.setValue(arrTempData, forKey: sectionString!)
                        print("Already in dict")
                    }
                    else {
                        let arr = NSMutableArray()
                        arr.add(dict)
                        arrKeys.add(sectionArray)
                        DicresData.setValue(arr, forKey: sectionString!)
                    }
                }
                
                if (Date as NSString).range(of: txtSearch.text!, options: .caseInsensitive).location != NSNotFound {
                    if DicresData[sectionArray] != nil {
                        let arrTempData = DicresData[sectionArray] as AnyObject
                        if arrTempData.contains(dict) {
                        }
                        else {
                            arrTempData.add(dict)
                        }
                        arrKeys.remove(sectionArray)
                        DicresData.removeObject(forKey: sectionArray)
                        arrKeys.add(sectionArray)
                        DicresData.setValue(arrTempData, forKey: sectionString!)
                        print("Already in dict")
                    }
                    else {
                        let arr = NSMutableArray()
                        arr.add(dict)
                        arrKeys.add(sectionArray)
                        DicresData.setValue(arr, forKey: sectionString!)
                    }
                }
                
//                if (Date as NSString).range(of: txtSearch.text!, options: .caseInsensitive).location != NSNotFound {
//                    if DicresData[sectionArray] != nil {
//                    }
//                    else {
//                        DicresData.setValue(arrData, forKey: sectionString!)
//                    }
//                }
            }
            
           
        }
        resData = DicresData.mutableCopy() as! NSDictionary
        
        if resData.count == 0 {
            lblNoResult.isHidden = false
            
        }
        else {
            lblNoResult.isHidden = true
        }
        tblView.reloadData()
    }
    
    @IBAction func clkToBackAction(_ sender: UIButton) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: HomeVC.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
    
    @IBAction func clkTobtnFilterAction(_ sender: UIButton) {
        if _userDefault.value(forKey: "resData") != nil {
            let dataarrCategory: Data? = _userDefault.data(forKey: "resData")
            resData = NSKeyedUnarchiver.unarchiveObject(with: dataarrCategory!) as! NSDictionary
            
            let dataKCategory: Data? = _userDefault.data(forKey: "arrKeys")
            arrKeys = NSKeyedUnarchiver.unarchiveObject(with: dataKCategory!) as! NSMutableArray
            
            txtSelectBus = UITextField(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
            txtSelectBus.backgroundColor = UIColor.clear
            self.view.addSubview(txtSelectBus)
            if resData.count > 0 {
                
                print(resData.count)
                
                //selectedValue = Array(resData.allKeys)[selectdRow] as! String
                selectedValue = arrKeys[selectdRow] as! String
                txtSelectBus.inputAccessoryView = toolBar
                txtSelectBus.inputView = pickerView
                pickerView.selectRow(selectdRow, inComponent: 0, animated: true)
                pickerView.reloadAllComponents()
                txtSelectBus.becomeFirstResponder()
            }
        }
    }
    
    // MARK:-  Picker Done Cancel Action Method
    // MARK: - ToolBar Button Action
    @objc func donePressed(){
        strFiltered = "YES"
        //txtSearch.text = ""
        print(selectdRow)
        print(selectedValue)
        self.view.endEditing(true)
        //let sectionString = Array(resData.allKeys)[selectdRow]
        let sectionString = arrKeys[selectdRow]
        let arrData = resData[sectionString] as AnyObject
        resData = [selectedValue : arrData]
        
        arrKeys = [selectedValue]
        
        let dataSave = NSKeyedArchiver.archivedData(withRootObject: resData)
        _userDefault.set(dataSave, forKey: "resTempData")
        
        let dataKeysSave = NSKeyedArchiver.archivedData(withRootObject: arrKeys)
        _userDefault.set(dataKeysSave, forKey: "KeysTempData")
        
        print(resData)
        
        self.txtSearchData()
        
        //tblView.reloadData()
    }
    
        @objc func cancelPressed(){
        self.view.endEditing(true) // or do something
        selectdRow = 0
        strFiltered = "NO"
        txtSearch.text = ""
        let dataarrCategory: Data? = _userDefault.data(forKey: "resData")
        resData = NSKeyedUnarchiver.unarchiveObject(with: dataarrCategory!) as! NSDictionary
        if resData.count == 0 {
            lblNoResult.isHidden = false
            
        }
        else {
            let dataarrCategory: Data? = _userDefault.data(forKey: "arrKeys")
            arrKeys = NSKeyedUnarchiver.unarchiveObject(with: dataarrCategory!) as! NSMutableArray
            
            lblNoResult.isHidden = true
        }
        tblView.reloadData()
        tblView.reloadData()
    }
    
    func dismiss(_ viewController: ReaderViewController) {
        self.navigationController!.popViewController(animated: true)
        self.dismiss(animated: true, completion:nil)
    }
    
}
