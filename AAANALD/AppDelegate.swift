//
//  AppDelegate.swift
//  AAANALD
//
//  Created by Gaurang Mistry on 27/04/18.
//  Copyright © 2018 Gaurang Mistry. All rights reserved.
//

import UIKit
import UserNotifications
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import Google
import GoogleToolboxForMac
import SDWebImage
import AVFoundation
import CoreFoundation
import CoreData
import Foundation

////////////

let app_Delegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate

extension UIApplication {
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {
    
    var UDIDVar = UIDevice.current.identifierForVendor!
    
    var window: UIWindow?
    var tracker: GAITracker?
    var viewShowLoad: UIView?
    var viewShowTost: UIView?
    var objSpinKit: RTSpinKitView?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        sleep(2)
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        //===========For Firebase Push Notification=================================================//
        Messaging.messaging().delegate = self
        
        //        if #available(iOS 10.0, *) {
        //
        //            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        //            UNUserNotificationCenter.current().requestAuthorization(
        //                options: authOptions,
        //                completionHandler: {_, _ in
        //                    print("notification")
        //
        //            })
        //            UNUserNotificationCenter.current().requestAuthorization(options: UNAuthorizationOptions.alert, completionHandler: { (true, error) in
        //                print("notification 11")
        //            })
        //            // For iOS 10 data message (sent via FCM
        //
        //        } else {
        let settings: UIUserNotificationSettings =
            UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
        application.registerUserNotificationSettings(settings)
        //}
        
        application.registerForRemoteNotifications()
        FirebaseApp.configure()
        //====================================================================================//
        
        self.setupGoogleAnalytics()
        
        return true
    }
    
    func setupGoogleAnalytics() {
        GAI.sharedInstance().trackUncaughtExceptions = true;
        GAI.sharedInstance().dispatchInterval = 20
        GAI.sharedInstance().logger.logLevel = .verbose
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            //UA-100068128-1
            appDelegate.tracker = GAI.sharedInstance().tracker(withTrackingId: "UA-110004453-1")
        }
    }
    
    // MARK: - CUSTOM LOADING METHODS
    func startLoadingview(_ strMessage: String) {
        /*
         DISPLAY CUSTOM LOADING SCREEN WHEN THIS METHOD CALLS.
         */
        // CREATE CUSTOM VIEW
        viewShowLoad = UIView()
        viewShowLoad?.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        viewShowLoad?.backgroundColor = UIColor.clear
        // SET THE VIEW INSIDE MAIN VIEW
        let viewUp = UIView(frame: (viewShowLoad?.frame)!)
        viewUp.backgroundColor = UIColor.black
        viewUp.alpha = 0.3
        viewShowLoad?.addSubview(viewUp)
        // CUSTOM ACTIVITY INDICATOR
        objSpinKit = RTSpinKitView(style: RTSpinKitViewStyle.bounce, color: UIColor.white)
        objSpinKit?.center = CGPoint(x: (viewShowLoad?.frame.midX)!, y: (viewShowLoad?.frame.midY)!)
        objSpinKit?.startAnimating()
        viewShowLoad?.addSubview(objSpinKit!)
        // SET THE LABLE
        let lblLoading = UILabel(frame: CGRect(x: 0, y: (objSpinKit?.frame.origin.y)! + 30, width: UIScreen.main.bounds.size.width, height: 50))
        lblLoading.font = UIFont(name: "Driod Sans", size: 14.0)
        lblLoading.text = strMessage
        lblLoading.backgroundColor = UIColor.clear
        lblLoading.textColor = UIColor.white
        lblLoading.textAlignment = .center
        viewShowLoad?.addSubview(lblLoading)
        window?.addSubview(viewShowLoad!)
    }
    
    func stopLoadingView() {
        /*
         REMOVE THE LOADING SCREEN WHEN THIS METHOD CALLS.
         */
        objSpinKit?.stopAnimating()
        viewShowLoad?.removeFromSuperview()
    }
    
    
    
    //===========================================================================================//
    //=============================Firebase Push Notification===================================//
    //===========================================================================================//
    func application(received remoteMessage: MessagingRemoteMessage) {
        print(remoteMessage.appData)
    }
    private func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        print("Device Token: \(deviceToken)")
    }
    
    internal func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        //playSound()
        print("receive notification")
    }
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
    }
    //    func messaging(_ messaging: messaging, didRefreshRegistrationToken fcmToken: String) {
    //        let token = fcmToken
    //        print("FCM tokem",token)
    //        _userDefault.set(token, forKey: "fcm")
    //        //Constant.showAlert(title: token as NSString, message: "FCM Token")
    //    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification data: [AnyHashable : Any]) {
        print("Push notification received: \(data)")
    }
    
    func application(_ application: UIApplication, didReceive notification: UILocalNotification) {
        //show an alert window
    }
    
    //print out all registed NSNotification for debug
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        print(notificationSettings.types.rawValue)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (_: UIBackgroundFetchResult) -> Void) {
        if UIApplication.shared.applicationState == .active {
            application.applicationIconBadgeNumber = 0;
            processAPN(userInfo)
            AudioServicesPlaySystemSound(1007)
        }
    }
    
    func processAPN(_ dict: [AnyHashable: Any]) {
        //    NSLog(@"NOTIFICATION :: %@", dict);
        let aps = dict["aps"] as? [AnyHashable: Any]
        if aps != nil {
            //        NSLog(@"%@", aps);
            Constant.showAlert(title: "AAANLD Notification", message: aps?["alert"] as! String)
        }
    }
    //===========================================================================================//
    //============//===================//===============//===================//==================//
    //===========================================================================================//
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        application.applicationIconBadgeNumber = 0;
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func makeaCall(_ phone_number: String) {
        var phNo: String = phone_number
        phNo = phNo.replacingOccurrences(of: " ", with: "")
        phNo = phNo.replacingOccurrences(of: "(", with: "")
        phNo = phNo.replacingOccurrences(of: ")", with: "")
        phNo = phNo.replacingOccurrences(of: "-", with: "")
        let phoneUrl = URL(string: "telprompt:\(phNo)")
        if (phNo.characters.count ) > 0 {
            if UIApplication.shared.canOpenURL(phoneUrl!) {
                UIApplication.shared.openURL(phoneUrl!)
            }
            else {
                let calert = UIAlertView(title: "Alert", message: "Call facility is not available!!!", delegate: nil, cancelButtonTitle: "OK")
                calert.show()
            }
        }
        else {
            let calert = UIAlertView(title: "Alert", message: "Number is not available!!!", delegate: nil, cancelButtonTitle: "OK")
            calert.show()
        }
    }
    
    func setImageColor(_ ImageName: UIImageView, strName: String, color: String) {
        // SET THE IMAGE
        ImageName.image = UIImage(named: strName)?.withRenderingMode(.alwaysTemplate)
        ImageName.tintColor = self.themeColor()
    }
    
    func setImageWhiteColor(_ ImageName: UIImageView, strName: String, color: String) {
        // SET THE IMAGE
        ImageName.image = UIImage(named: strName)?.withRenderingMode(.alwaysTemplate)
        ImageName.tintColor = UIColor.white
    }
    
    func setImageGrayColor(_ ImageName: UIImageView, strName: String, color: String) {
        // SET THE IMAGE
        ImageName.image = UIImage(named: strName)?.withRenderingMode(.alwaysTemplate)
        ImageName.tintColor = UIColor.lightGray
    }
    
    func themeColor()->UIColor{
        return UIColor(red:146/255.0, green:85/255.0, blue:71/255.0, alpha: 1.0)
    }
}

extension Data {
    func hexString() -> String {
        return self.reduce("") { string, byte in
            string + String(format: "%02X", byte)
        }
    }
}
