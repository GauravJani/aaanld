//
//  ContactVC.swift
//  NewApp
//
//  Created by Gaurang Mistry on 16/10/17.
//  Copyright © 2017 Gaurang Mistry. All rights reserved.
//

import UIKit
import Google

class ContactVC: UIViewController, UITextFieldDelegate, UITextViewDelegate, ServerCallDelegate {

    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtSeconds: UITextField!
    @IBOutlet weak var txtAsunto: UITextField!
    @IBOutlet weak var txtMessage: UITextView!
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var constraint_ViewBottomHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        txtName.delegate = self
        txtSeconds.delegate = self
        txtAsunto.delegate = self
        txtMessage.delegate = self
        print("Did Load Method")
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("Will appear method")
        //=======================Goolge Analytics======================================//
        print("Screen Name---------->>", kFifthOptions)
        guard let tracker = GAI.sharedInstance().defaultTracker else { return }
        tracker.set(kGAIScreenName, value: kFifthOptions)
        guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
        tracker.send(builder.build() as [NSObject : AnyObject])
        //======================================================================//
    }
    
    // MARK: - Submit Contacto Api Call Method
    func CallApiForSubmitContacto() {
        // init paramters Dictionary
        let myUrl = kBasePath + "addContact"
        
        let param = ["first_name" : txtName.text!,
                     "email"      : txtSeconds.text!,
                     "subject"    : txtAsunto.text!,
                     "message"    : txtMessage.text!]
        
        print(myUrl, param)
        
        ServerCall.sharedInstance.requestWithUrlAndParameters(.POST, urlString: myUrl, parameters: param as [String : AnyObject], delegate: self, name: .serverCallNameAddContact)
    }
    
    
    // MARK: - Server Call Delegate
    func ServerCallSuccess(_ resposeObject: AnyObject, name: ServerCallName) {
        print(resposeObject)
        var dicData = resposeObject as! [AnyHashable : Any]
        
        if name == ServerCallName.serverCallNameAddContact {
            
            if ((dicData["success"]) != nil) {
                app_Delegate.stopLoadingView()
                // Create the alert controller
                let strResponse = TO_INT(dicData["success"]);
                let strMsg = TO_STRING(dicData["msg"]);
                
                if strResponse == 1 {
                    
                    let alertController = UIAlertController(title: "", message: strMsg, preferredStyle: .alert)
                    
                    // Create the actions
                    let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
                        UIAlertAction in
                        _ = self.navigationController?.popViewController(animated: true)
                        NSLog("OK Pressed")
                    }
                    
                    // Add the actions
                    alertController.addAction(okAction)
                    
                    // Present the controller
                    self.present(alertController, animated: true, completion: nil)
                    
                }
                else {
                    app_Delegate.stopLoadingView()
                    Constant.showAlert(title: "", message: strMsg)
                    return
                }
            }
            else {
                let strerrMsg = TO_STRING(dicData["error"])
                Constant.showAlert(title: "", message:strerrMsg)
            }
        }
        app_Delegate.stopLoadingView()
    }
    
    
    // MARK: - Server Failed Delegate
    func ServerCallFailed(_ errorObject: String, name: ServerCallName) {
        app_Delegate.stopLoadingView()
        Constant.showAlert(title: "", message:errorObject)
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    // MARK: - TextField Delegate Method
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtName {
            textField.resignFirstResponder()
            txtSeconds.becomeFirstResponder()
            return false
        }
        else if textField == txtSeconds {
            textField.resignFirstResponder()
            txtAsunto.becomeFirstResponder()
            return false
        }
        else if textField == txtAsunto {
            textField.resignFirstResponder()
            txtMessage.becomeFirstResponder()
            return false
        }
        textField.resignFirstResponder()
        return true
    }
    
    // MARK: - Validation function
    func isValidData() -> Bool{
        if !(txtName?.text!.isStringWithoutSpace())!{
            Constant.showAlert(title: "", message: "Por favor ingrese el nombre" )
            return false;
        }
        if !(txtSeconds?.text!.isStringWithoutSpace())!{
            Constant.showAlert(title: "", message: "Por favor ingrese correo electrónico" )
            return false;
        }
        else if !(txtSeconds?.text!.isEmail())!{
            Constant.showAlert(title: "", message: "Por favor introduzca un correo electrónico válido")
            return false;
        }
        return true
    }
    
    
    @IBAction func clkToSubmitContactUSAction(_ sender: UIButton) {
        if isValidData() == false {
            return
        }
        else{
            self.view.endEditing(true)
            //Call Api For Login
            app_Delegate.startLoadingview("")
            self.CallApiForSubmitContacto()
        }
    }
    
    
    @IBAction func clkToBackAction(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }

}





